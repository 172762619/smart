package com.smart.security.init;

import javax.servlet.ServletContext;

public interface ISmartInitializer {

    void init(ServletContext servletContext);
}
