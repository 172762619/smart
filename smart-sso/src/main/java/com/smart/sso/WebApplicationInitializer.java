package com.smart.sso;

import javax.servlet.ServletContext;

public interface WebApplicationInitializer {

    void init(ServletContext servletContext);
}
