package com.smart.framework;

public interface Plugin {

    void init();

    void destroy();
}
